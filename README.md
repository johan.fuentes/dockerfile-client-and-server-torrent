# Live torrent

Busque, explore y descargue archivos torrent en línea.

Vea películas de YTS en línea con subtítulos.

## Para construir los contenedores ejecutar los siguientes comandos:

sudo docker build .

sudo docker run -it  id_container

## Para correr el cliente

cd live-torrent && npm start

## Para correr el servidor

cd live-torrent-backend && npm start

## Conocer en la ip asociada al contenedor

sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' id_container

## Video del funcionamiento del servidor y el cliente, capturando el trafico generado por ellos en wireshark
[![Watch the video](images/videoPlay.png)](https://www.youtube.com/watch?v=Q9Ljh-_8g0U)



